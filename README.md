<p align="center">
<img src="https://upload.wikimedia.org/wikipedia/commons/a/a6/Landlords_board_game_cover.jpg" alt="Wiki" width="600"/>
</p>

# Landlord's Game

The Landlord's Game is a board game patented in 1904 by Elizabeth Magie as U.S. Patent 748,626. It is a realty and taxation game intended to educate users about Georgism. It is the inspiration for the 1935 board game Monopoly. (Wikipedia)