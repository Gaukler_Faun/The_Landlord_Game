### Rules and background

- U.S. Patent 748,626: https://patents.google.com/patent/US748626
- https://landlordsgame.info/games/lg-1906/lg-1906_egc-rules.html
- https://en.wikipedia.org/wiki/The_Landlord%27s_Game

### Pictures for the board

- _Jail_: https://commons.wikimedia.org/wiki/File:Moyamensing_Prison_-_Philadelphia_And_Its_Environs_1875.jpg

- _Estate_: https://upload.wikimedia.org/wikipedia/commons/3/3a/Ansicht_des_Lustschlosses_Gallitzin_in_Wien_um_1810_OeNB_12981484.jpg

- _Central Park_: https://commons.wikimedia.org/wiki/File:View_in_Central_Park_with_Balcony_Bridge_and_Oak_Bridge_(Valentine%27s_Manual)_MET_DP823429.jpg

- _Mother earth_: https://commons.wikimedia.org/wiki/File:Dominque_Louis_Papety_-_Sleeping_Field_Worker_-_2010.173_-_Cleveland_Museum_of_Art.tif

- _Map_: https://commons.wikimedia.org/wiki/File:1862_Johnson_Map_of_New_York_City_and_Brooklyn_-_Geographicus_-_NYC-johnson-1862.jpg